var selectElementsStartingWithA = function(array) {
	var tab = [];
	for (var i = 0; i < array.length; i++) {
		if (array[i].charAt(0) == 'a') {
			tab.push(array[i]);
		}
	}
	return tab;
}

var selectElementsStartingWithVowel = function(array) {
	var tab = [];
	var voyelle = ['a','e','i','o','u','y'];
	for (var i = 0; i < array.length; i++) {
		for (var j = 0; j < voyelle.length; j++) {
			if (array[i].charAt(0) == voyelle[j]) {
				tab.push(array[i]);
			}
		}
	}
	return tab;
}

var removeNullElements = function(array) {
	for (var i = 0; i < array.length; i++) {
		if (array[i] == null) {
			array.splice(i, 1);
			i = i-1;
		}
	}
	return array;
}

var removeNullAndFalseElements = function(array) {
	for (var i = 0; i < array.length; i++) {
		if (array[i] === null || array[i] === false) {
			array.splice(i, 1);
			i = i-1;
		}
	}
	return array;
}

var reverseWordsInArray = function(array) {
	for (var i = 0; i < array.length; i++) {
		array[i] = array[i].split("").reverse().join("");
	}
	return array;
}

var everyPossiblePair = function(array) {
	var result = [];
	for (i = 0; i < array.length; i++) {
	  for (j = i + 1; j < array.length; j++) {
	    result.push([array[i], array[j]]);
	  }
	}
	return result;
}

var allElementsExceptFirstThree = function(array) {
	for (i = 2; i >= 0; i--) {
		array.splice(i, 1);
	}
	return array;
}

var addElementToBeginning = function(array, element) {
	array.unshift(element);
	return array;
}

var sortByLastLetter = function(array) {
	for (var i = 0; i < array.length; i++) {
		array[i] = array[i].split("").reverse().join("");
	}
	array.sort();
	for (var i = 0; i < array.length; i++) {
		array[i] = array[i].split("").reverse().join("");
	}
	return array;
}

var getFirstHalf = function(string) {
	var retour = [];

	var half = (string.length % 2 == 0 ? string.length / 2 : Math.round(string.length / 2));

	string = string.trim().split("");

	for (var i = 0; i < half; i++) {
		retour.push(string[i]);
	}

	retour = retour.join("");
	return retour;
}

var makeNegative = function(number) {
	number = -Math.abs(number);
	return number;
}

var numberOfPalindromes = function(array) {
	j = 0;
	compare = [];
	for (var i = 0; i < array.length; i++) {
		compare[i] = array[i].split("").reverse().join("");
		if(compare[i] == array[i]){
			j++;
		}
	}
	return j;
}

var shortestWord = function(array) {
	var lg = 1000;
	var shortest;

	for(var i=0; i < array.length; i++){
		if(array[i].length < lg){
			var lg = array[i].length;
			shortest = array[i];
		}
	}
	return shortest;
}

var longestWord = function(array) {
	var lg = 0;
	var longest;

	for(var i=0; i < array.length; i++){
		if(array[i].length > lg){
			var lg = array[i].length;
			longest = array[i];
		}
	}
	return longest;
}

var sumNumbers = function(array) {
	var sum = 0;

	for (var i = 0; i < array.length; i++) {
		sum += array[i];
	}

	return sum;
}

var repeatElements = function(array) {
	lg = array.length;

	for (var i = 0; i < lg; i++) {
		array.push(array[i]);
	}
	return array;
}

var stringToNumber = function(string) {
    return Number(string);
}

var calculateAverage = function(array) {
	var div = array.length;
	var sum = 0;
	var average = 0;
	for (var i = 0; i < array.length; i++) {
		sum += array[i];
	}
	return average = sum/div;
}

var getElementsUntilGreaterThanFive = function(array) {
	retour = [];
	for (var i = 0; i < array.length; i++) {
		if(Number(array[i]) > 5)
		{
			break;
		}
		else
		{
			retour[i] = array[i];
		}
	}
	return retour;
}

var convertArrayToObject = function(array) {
	var obj = {};
	for (var i = 0; i < array.length; i=i+2){
		obj[array[i]] = array[i+1];
	}

  return obj;
}

var getAllLetters = function(array) {
	letters = "";
	for (var i = 0; i < array.length; i++) {
		letters += array[i].split(",");
	}
	letters = letters.split("").sort();

	for (var i = 0; i < letters.length; i++) {
		if(letters[i] == letters[i+1]){
			letters.splice(i,1);
			i--;
		}
	}
	return letters;
}

var swapKeysAndValues = function(object) {
	var new_obj = {};

  for (var prop in object) {
    if(object.hasOwnProperty(prop)) {
      new_obj[object[prop]] = prop;
    }
  }

  return new_obj;
}

var sumKeysAndValues = function(object) {
	var sum = 0;
	for (var prop in object) {
    if(object.hasOwnProperty(prop)) {
    	sum += Number(prop)+Number(object[prop]);
    }
  }
	return sum;
}

var removeCapitals = function(string) {
	string = string.replace(/[A-Z]/g, '');
	return string;
}

var roundUp = function(number) {
	return Math.ceil(number);
}

var formatDateNicely = function(date) {
	var options = {year: "numeric", month: "numeric", day: "numeric"};
	return hello = date.toLocaleString("fr-FR", options);
}

var getDomainName = function(string) {
	string = string.replace(/([a-z]+)@/,'');
	return string = string.replace(/.com/,'');
}

var titleize = function(string) {
	return 'Write your method here';
}

var checkForSpecialCharacters = function(string) {
	return 'Write your method here';
}

var squareRoot = function(number) {
	return 'Write your method here';
}

var factorial = function(number) {
	return 'Write your method here';
}

var findAnagrams = function(string) {
	return 'Write your method here';
}

var convertToCelsius = function(number) {
	return 'Write your method here';
}

var letterPosition = function(array) {
	return 'Write your method here';
}
